@echo off
if not exist .\build (
  mkdir build
)
cd build
@echo on
cmake -G "Visual Studio 17 2022" -A x64 ..
@echo off
cd ..
