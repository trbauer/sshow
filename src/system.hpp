#pragma once

#include "text.hpp"

void fatal_handler(const char* msg);

template <typename...Ts>
[[noreturn]]
void fatal(Ts... ts){
  fatal_handler(format(ts...).c_str());
}