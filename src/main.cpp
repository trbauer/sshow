#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <cstdio>
#include <iostream>
#include <random>
#include <string>
#include <sstream>

#include "artifacts/resources.hpp"
#include "system.hpp"
#include "stats.hpp"
#include "text.hpp"

/*
NEXT STEPS
  ==> set transparency

  ==> sf::Sprites

  ==> selectable ships (for debuggin)
    - need a texture shape with stipple lines for selected ships
    - unselected ships do what?


   create N friendly and M enemy ships (Sprites)
     create random ships https://en.cppreference.com/w/cpp/numeric/random
     rotate enemy 180 deg

   class Ship encapsulates
    - rotation, position, velocity, hitpoints, goal


  - add engine flame sprites during acceleration


  - make ships selectable (hover shows info)
    drawText...
  - give ships health bars


  ==> popup motes that show info (e.g. name, course, ...)

- 'p' dumps profile times (record times for various parts of the frame)



/// C++20
use std::format https://en.cppreference.com/w/cpp/utility/format/format
use numbers for constants https://en.cppreference.com/w/cpp/header/numbers

BUGS:
 - flickering texture border
     OPTIONS:
       - remove borders in PNG and hope for the best
       - create MRE and slide things along until I find a bad
         value that exhibits the problem

 - transparency
    (setup a pair of ships to overlap and illustrate)
    https://appuals.com/ms-paint-turn-white-background-transparent/

*/
constexpr double PI = 3.14159265358979323846;

struct Ship {
  const sf::Sprite *sprite;

  float sX = 0.0f, sY = 0.0f;
  float vX = 0.0f, vY = 0.0f;
  float rot = 0.0f;

  Ship(const sf::Sprite *s)
    : sprite(s)
  { }

}; // Ship

struct State {
  sf::Font sansFont;

  sf::Clock clock;
  size_t frameIndex = 0;

  const sf::VideoMode desktopMode = sf::VideoMode::getDesktopMode();
  bounded_sampler<64> fpsSamples;

  sf::RenderWindow* w = nullptr;

  int windowStyle = sf::Style::Default;

  sf::Texture artTexture;
  sf::Sprite *blueShip;
  sf::Sprite *redStation;

  std::vector<Ship> blueShips;

  sf::RenderWindow *createWindow(int style, unsigned w, unsigned h) {
    sf::RenderWindow* wnd =
      new sf::RenderWindow(
        sf::VideoMode(w, h, desktopMode.bitsPerPixel), "ShipShow", style);
    return wnd;
  };

  State() : w(createWindow(sf::Style::Default, 800, 800)) {
    if (!sansFont.loadFromMemory(res::font_sansation, res::font_sansation_len)) {
      fatal("failed to load font");
    }
    if (!artTexture.loadFromMemory(res::png_ships, res::png_ships_len)) {
      fatal("failed to load images");
    }
    // artTexture.setSmooth(true);
    // ships are 20px apart
    // TOP LEFT  ...
    // (28, 42)  (48, 42) ...
    // (28, 62)
    // Spacing between colors
    //
    // DX to next sheet is 110
    // Green ships are (138,42)...
    // Red ships (248,42)
    // Yellow
    // Purple
    enum class Team {BLUE, GREEN, RED, YELLOW, PINK};
    auto loadShipSprite = [&](Team p, int x, int y) {
      int px = 28 + 20 * x + 110 * int(p);
      int py = 42 + 20 * y;
      auto *s = new sf::Sprite(artTexture, sf::IntRect(px, py, 16, 16));
      return s;
    };
    // looks like a battleship
    blueShip = loadShipSprite(Team::BLUE, 2, 8);
    blueShip->setPosition(200.0f, 200.0f);
    blueShip->setOrigin(7, 7);
    blueShip->setScale(1.0f, 1.0f);
    // space station
    redStation = loadShipSprite(Team::RED, 3, 18);
    redStation->setPosition(500.0f, 500.0f);
    redStation->setScale(5.0f, 5.0f);
    // dx = 20
  }

  template <typename...Ts>
  void drawText(float x, float y, Ts...ts) {
    if (!w) {
      return;
    }
    auto fmt = format(ts...);
    sf::Text txt(fmt.c_str(), sansFont);
    txt.setPosition(x, y);
    // txt.setFillColor(sf::Color::Red);
    w->draw(txt);
  }

  void loop() {
    const float DAMPING = 0.99999f;
    const float THRUST = 100.0f;
    const float TO_RADIANS = (float)PI / 180.0f;

    float rot = 0.0f;
    float vX = 0.0f, vY = 0.0f;
    float sX = 200.0f, sY = 200.0f;

    auto lastT = clock.getElapsedTime();
    while (w->isOpen()) {
      // clear the window with black color
      w->clear(sf::Color::Black);

      auto t = clock.getElapsedTime();
      float dt = (t - lastT).asSeconds();
      lastT = t;

      fpsSamples.add(dt);
      drawText(10.0f, 10.0f, "TIME: ", t.asSeconds());
      auto fps = 1.0 / fpsSamples.avg();
      auto test = format(frac(fps, 0, 3));
      drawText(10.0f, 40.0f, "FRAME: ", frameIndex,
               " (avg ", frac(fps, 5, 1), " fps)");
      drawText(10.0f, 70.0f, "vel <", frac(vX, 7, 3), ",",
               frac(vY, 7, 3), ">");

      sX += dt * vX;
      sY += dt * vY;
      vX *= DAMPING; vY *= DAMPING;

      sX = std::clamp(sX, 0.0f, (float)w->getSize().x);
      sY = std::clamp(sY, 0.0f, (float)w->getSize().y);

      blueShip->setPosition(sX, sY);
      blueShip->setRotation(rot);

      w->draw(*blueShip);
      w->draw(*redStation);

      // end the current frame
      w->display();

      sf::Event e;
      while (w->pollEvent(e)) {
        if (e.type == sf::Event::Closed)
          w->close();
        else if (e.type == sf::Event::KeyPressed) {
          if (e.key.code == sf::Keyboard::Escape) {
            w->close();
          } else if (e.key.code == sf::Keyboard::F11) {
            auto wi = w->getSize().x;
            auto ht = w->getSize().y;
            w->close();
            delete w;
            windowStyle ^= sf::Style::Fullscreen;
            w = createWindow(windowStyle, wi, ht);
          } else if (e.key.code == sf::Keyboard::Left) {
            rot -= 45.0f;
            std::cout << "rot: " << rot << "\n";
          } else if (e.key.code == sf::Keyboard::Right) {
            rot += 45.0f;
            std::cout << "rot: " << rot << "\n";
          } else if (e.key.code == sf::Keyboard::Up) {
            vX += THRUST * std::cosf(TO_RADIANS * rot);
            vY += THRUST * std::sinf(TO_RADIANS * rot);
            std::cout << "THRUST * std::cosf(" << TO_RADIANS * rot << "): "
                      << THRUST * std::cosf(TO_RADIANS * rot) << "\n";
            std::cout << "THRUST * std::sin(" << TO_RADIANS * rot << "): "
                      << THRUST * std::sinf(TO_RADIANS * rot) << "\n";
          } else if (e.key.code == sf::Keyboard::Down) {
            vX -= THRUST * std::cosf(TO_RADIANS * rot);
            vY -= THRUST * std::sinf(TO_RADIANS * rot);
            std::cout << "THRUST * std::cosf(" << TO_RADIANS * rot << "): "
                      << THRUST * std::cosf(TO_RADIANS * rot) << "\n";
            std::cout << "THRUST * std::sin(" << TO_RADIANS * rot << "): "
                      << THRUST * std::sinf(TO_RADIANS * rot) << "\n";
          } else if (e.key.code == sf::Keyboard::D) {
            std::cout << "pos: <" << sX << "," << sY << ">\n";
            std::cout << "vel: <" << vX << "," << vY << ">\n";
          } else {
            std::cerr << "unhandled key 0x" << fmtHex(e.key.code) << "\n";
          }
          // sf::Keyboard::isKeyPressed

        } else if (e.type == sf::Event::KeyReleased) {
          ; // no op
        } else if (e.type == sf::Event::MouseMoved) {
          ; // no op
        } else {
          std::cerr << "unhandled event kind " << (int)e.type << "\n";
        }
      } // poll

      frameIndex++;
    }
    delete w;
    w = nullptr;
  } // loop
}; // struct State

int main(int argc, char **argv)
{
  State st;
  st.loop();

  return 0;
}

