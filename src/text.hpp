#pragma once

#include <cstring>
#include <cstdint>
#include <iomanip>
#include <ostream>
#include <sstream>


struct frac {
  float val;
  int sdigs, fdigs;
  frac(float v, int si, int fr) : val(v), sdigs(si), fdigs(fr) { }
};
struct hex {
  uint64_t value;
  int w;
  hex(uint64_t v, int _w = 0) : value(v), w(_w) { }
};

std::ostream& operator <<(std::ostream& os, const frac& f);
std::ostream& operator <<(std::ostream& os, const hex& h);


template <typename...Ts>
void          format_to(std::ostream&) { }
template <typename T, typename...Ts>
void          format_to(std::ostream& os, T t, Ts...ts) {
  os << t; format_to(os, ts...);
}
template <typename...Ts>
std::string   format(Ts...ts) {
  std::stringstream ss; format_to(ss, ts...); return ss.str();
}


static bool streq(const char* str1, const char* str2) {
  return str1 == str2 || (str1 && str2 && strcmp(str1, str2) == 0);
}
static bool strpfx(const char* pfx, const char* str) {
  return strncmp(str, pfx, strlen(pfx)) == 0;
}

template <typename T>
static std::string fmtHex(T val, int w = 2 * sizeof(T)) {
  std::stringstream ss;
  ss << std::uppercase << std::hex << std::setfill('0') << std::setw(w) << val;
  return ss.str();
}
