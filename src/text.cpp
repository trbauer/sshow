#include "text.hpp"

#include <iomanip>

std::ostream& operator <<(std::ostream& os, const frac& f)
{
  std::stringstream ss;
  ss <<
    std::fixed <<
    std::setprecision(f.fdigs) << std::setw(f.sdigs) << f.val;
  os << ss.str();
  return os;
}

std::ostream& operator <<(std::ostream& os, const hex& h)
{
  os << "0x" << fmtHex(h.value, h.w);
  return os;
}
