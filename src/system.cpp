#include <iostream>
#include <string>

#include "system.hpp"

void fatal_handler(const char* c_msg) {
  std::string msg = c_msg;
  if (!msg.empty() && msg[msg.size() - 1] != '\n')
    msg += "\n";
  if (msg.empty())
    msg = "<UNKNOWN ERROR>\n";
  std::cerr << msg;
  std::cerr.flush();
  exit(1);
}
