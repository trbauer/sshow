#pragma once

namespace res {
extern const unsigned char font_mono[];
extern const unsigned int font_mono_len;
extern const unsigned char font_sansation[];
extern const unsigned int font_sansation_len;

extern const unsigned char png_ships[];
extern const unsigned int png_ships_len;
}
